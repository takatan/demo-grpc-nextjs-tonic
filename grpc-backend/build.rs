fn main() -> Result<(), Box<dyn std::error::Error>> {
    tonic_build::compile_protos("hello.proto")?;
    println!("build hello.proto");
    Ok(())
}
