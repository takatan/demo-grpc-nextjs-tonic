use std::env;

use sea_orm::entity::*;
use sea_orm::ActiveValue::Set;
use sea_orm::{Database, QueryOrder};
use sea_orm::{DatabaseConnection, QuerySelect};
use tonic::{transport::Server, Request, Response, Status};

use hello::greeting_server::{Greeting, GreetingServer};
use hello::{HelloRequest, HelloResponse};

use crate::entity::message;
use crate::entity::prelude::Message;
use crate::hello::{MessageRecord, MessagesRequest, MessagesResponse};

mod entity;

pub mod hello {
    tonic::include_proto!("hello");
}

#[derive(Debug, Default)]
pub struct MyGreeting {
    db: DatabaseConnection,
}

#[tonic::async_trait]
impl Greeting for MyGreeting {
    async fn hello(
        &self,
        request: Request<HelloRequest>,
    ) -> Result<Response<HelloResponse>, Status> {
        println!("Got a request: {:?}", request);

        let text = request.into_inner().name;
        let m = message::ActiveModel {
            id: NotSet,
            user_name: Set("anonymous".to_owned()),

            text: Set(text.clone()),
        };
        m.insert(&self.db).await.unwrap();
        let response = HelloResponse {
            message: format!("PiyoPiyo  {}!", text).into(),
        };

        Ok(Response::new(response))
    }

    async fn messages(
        &self,
        _request: Request<MessagesRequest>,
    ) -> Result<Response<MessagesResponse>, Status> {
        let messages: Vec<message::Model> = Message::find()
            .order_by_desc(message::Column::Id)
            .limit(10)
            .all(&self.db)
            .await
            .unwrap();
        let response = MessagesResponse {
            messages: messages
                .iter()
                .map(|e| MessageRecord {
                    id: e.id,
                    user_name: e.user_name.clone(),
                    text: e.text.clone(),
                })
                .collect(),
        };
        println!("{:?}", response);
        Ok(Response::new(response))
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    let db = Database::connect(env::var("DATABASE_URL")?).await?;

    let addr = "[::1]:50051".parse()?;

    let my_greeting = MyGreeting { db: db.clone() };
    Server::builder()
        .add_service(GreetingServer::new(my_greeting))
        .serve(addr)
        .await?;

    Ok(())
}
