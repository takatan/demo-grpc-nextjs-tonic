import { HelloRequest } from '../codegen/hello_pb';
import { GreetingClient } from '../codegen/hello_grpc_pb';
import { credentials } from '@grpc/grpc-js';

const Request = new HelloRequest();
const Client = new GreetingClient('localhost:50051', credentials.createInsecure());

const Grpc = () => {
  Client.hello(Request, (grpcErr, grpcRes) => {
    if (grpcErr) {
      // setMsg('ERROR');
    } else {
      // setMsg(grpcRes.getMessage());
    }
  });
  return <div>aaa</div>;
};

export default Grpc;
