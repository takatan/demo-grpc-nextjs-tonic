// package: hello
// file: hello.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class HelloRequest extends jspb.Message { 
    getName(): string;
    setName(value: string): HelloRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HelloRequest.AsObject;
    static toObject(includeInstance: boolean, msg: HelloRequest): HelloRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HelloRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HelloRequest;
    static deserializeBinaryFromReader(message: HelloRequest, reader: jspb.BinaryReader): HelloRequest;
}

export namespace HelloRequest {
    export type AsObject = {
        name: string,
    }
}

export class HelloResponse extends jspb.Message { 
    getMessage(): string;
    setMessage(value: string): HelloResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): HelloResponse.AsObject;
    static toObject(includeInstance: boolean, msg: HelloResponse): HelloResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: HelloResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): HelloResponse;
    static deserializeBinaryFromReader(message: HelloResponse, reader: jspb.BinaryReader): HelloResponse;
}

export namespace HelloResponse {
    export type AsObject = {
        message: string,
    }
}

export class MessageRecord extends jspb.Message { 
    getId(): number;
    setId(value: number): MessageRecord;
    getUserName(): string;
    setUserName(value: string): MessageRecord;
    getText(): string;
    setText(value: string): MessageRecord;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MessageRecord.AsObject;
    static toObject(includeInstance: boolean, msg: MessageRecord): MessageRecord.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MessageRecord, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MessageRecord;
    static deserializeBinaryFromReader(message: MessageRecord, reader: jspb.BinaryReader): MessageRecord;
}

export namespace MessageRecord {
    export type AsObject = {
        id: number,
        userName: string,
        text: string,
    }
}

export class MessagesRequest extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MessagesRequest.AsObject;
    static toObject(includeInstance: boolean, msg: MessagesRequest): MessagesRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MessagesRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MessagesRequest;
    static deserializeBinaryFromReader(message: MessagesRequest, reader: jspb.BinaryReader): MessagesRequest;
}

export namespace MessagesRequest {
    export type AsObject = {
    }
}

export class MessagesResponse extends jspb.Message { 
    clearMessagesList(): void;
    getMessagesList(): Array<MessageRecord>;
    setMessagesList(value: Array<MessageRecord>): MessagesResponse;
    addMessages(value?: MessageRecord, index?: number): MessageRecord;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): MessagesResponse.AsObject;
    static toObject(includeInstance: boolean, msg: MessagesResponse): MessagesResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: MessagesResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): MessagesResponse;
    static deserializeBinaryFromReader(message: MessagesResponse, reader: jspb.BinaryReader): MessagesResponse;
}

export namespace MessagesResponse {
    export type AsObject = {
        messagesList: Array<MessageRecord.AsObject>,
    }
}
