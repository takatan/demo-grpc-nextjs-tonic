// package: hello
// file: hello.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "@grpc/grpc-js";
import * as hello_pb from "./hello_pb";

interface IGreetingService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    hello: IGreetingService_IHello;
    messages: IGreetingService_IMessages;
}

interface IGreetingService_IHello extends grpc.MethodDefinition<hello_pb.HelloRequest, hello_pb.HelloResponse> {
    path: "/hello.Greeting/Hello";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<hello_pb.HelloRequest>;
    requestDeserialize: grpc.deserialize<hello_pb.HelloRequest>;
    responseSerialize: grpc.serialize<hello_pb.HelloResponse>;
    responseDeserialize: grpc.deserialize<hello_pb.HelloResponse>;
}
interface IGreetingService_IMessages extends grpc.MethodDefinition<hello_pb.MessagesRequest, hello_pb.MessagesResponse> {
    path: "/hello.Greeting/Messages";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<hello_pb.MessagesRequest>;
    requestDeserialize: grpc.deserialize<hello_pb.MessagesRequest>;
    responseSerialize: grpc.serialize<hello_pb.MessagesResponse>;
    responseDeserialize: grpc.deserialize<hello_pb.MessagesResponse>;
}

export const GreetingService: IGreetingService;

export interface IGreetingServer extends grpc.UntypedServiceImplementation {
    hello: grpc.handleUnaryCall<hello_pb.HelloRequest, hello_pb.HelloResponse>;
    messages: grpc.handleUnaryCall<hello_pb.MessagesRequest, hello_pb.MessagesResponse>;
}

export interface IGreetingClient {
    hello(request: hello_pb.HelloRequest, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    hello(request: hello_pb.HelloRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    hello(request: hello_pb.HelloRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    messages(request: hello_pb.MessagesRequest, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
    messages(request: hello_pb.MessagesRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
    messages(request: hello_pb.MessagesRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
}

export class GreetingClient extends grpc.Client implements IGreetingClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: Partial<grpc.ClientOptions>);
    public hello(request: hello_pb.HelloRequest, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    public hello(request: hello_pb.HelloRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    public hello(request: hello_pb.HelloRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: hello_pb.HelloResponse) => void): grpc.ClientUnaryCall;
    public messages(request: hello_pb.MessagesRequest, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
    public messages(request: hello_pb.MessagesRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
    public messages(request: hello_pb.MessagesRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: hello_pb.MessagesResponse) => void): grpc.ClientUnaryCall;
}
