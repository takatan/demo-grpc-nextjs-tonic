// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var hello_pb = require('./hello_pb.js');

function serialize_hello_HelloRequest(arg) {
  if (!(arg instanceof hello_pb.HelloRequest)) {
    throw new Error('Expected argument of type hello.HelloRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hello_HelloRequest(buffer_arg) {
  return hello_pb.HelloRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hello_HelloResponse(arg) {
  if (!(arg instanceof hello_pb.HelloResponse)) {
    throw new Error('Expected argument of type hello.HelloResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hello_HelloResponse(buffer_arg) {
  return hello_pb.HelloResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hello_MessagesRequest(arg) {
  if (!(arg instanceof hello_pb.MessagesRequest)) {
    throw new Error('Expected argument of type hello.MessagesRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hello_MessagesRequest(buffer_arg) {
  return hello_pb.MessagesRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_hello_MessagesResponse(arg) {
  if (!(arg instanceof hello_pb.MessagesResponse)) {
    throw new Error('Expected argument of type hello.MessagesResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_hello_MessagesResponse(buffer_arg) {
  return hello_pb.MessagesResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var GreetingService = exports.GreetingService = {
  hello: {
    path: '/hello.Greeting/Hello',
    requestStream: false,
    responseStream: false,
    requestType: hello_pb.HelloRequest,
    responseType: hello_pb.HelloResponse,
    requestSerialize: serialize_hello_HelloRequest,
    requestDeserialize: deserialize_hello_HelloRequest,
    responseSerialize: serialize_hello_HelloResponse,
    responseDeserialize: deserialize_hello_HelloResponse,
  },
  messages: {
    path: '/hello.Greeting/Messages',
    requestStream: false,
    responseStream: false,
    requestType: hello_pb.MessagesRequest,
    responseType: hello_pb.MessagesResponse,
    requestSerialize: serialize_hello_MessagesRequest,
    requestDeserialize: deserialize_hello_MessagesRequest,
    responseSerialize: serialize_hello_MessagesResponse,
    responseDeserialize: deserialize_hello_MessagesResponse,
  },
};

exports.GreetingClient = grpc.makeGenericClientConstructor(GreetingService);
