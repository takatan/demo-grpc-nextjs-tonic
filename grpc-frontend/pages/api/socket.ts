import { NextApiRequest, NextApiResponse } from 'next';
import { Socket } from 'net';
import { Server as SocketIOServer } from 'socket.io';
import { Server } from 'http';

export type NextApiResponseServerIO<T = any> = NextApiResponse<T> & {
  socket: Socket & {
    server: Server & {
      io: SocketIOServer;
    };
  };
};

export default function handler(req: NextApiRequest, res: NextApiResponseServerIO) {
  if (!res.socket.server.io) {
    console.log('New Socket.io server...');
    res.socket.server.io = new SocketIOServer(res.socket.server);
    res.socket.server.io.on('connection', (socket) => {
      socket.on('input-change', (msg) => {
        socket.broadcast.emit('update-input', `b: ${msg}`);
        socket.emit('update-input', `e: ${msg}`);
      });
    });
  }
  res.end();
}
