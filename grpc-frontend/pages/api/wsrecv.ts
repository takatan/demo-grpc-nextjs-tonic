import type { NextApiRequest } from 'next';
import { HelloRequest } from '../../codegen/hello_pb';
import { GreetingClient } from '../../codegen/hello_grpc_pb';
import { credentials } from '@grpc/grpc-js';
import { NextApiResponseServerIO } from './socket';

export default function handler(req: NextApiRequest, res: NextApiResponseServerIO<{}>) {
  const x: Partial<{ name: string }> = req.query;

  const Request = new HelloRequest().setName(x.name || 'empty');
  const Client = new GreetingClient('localhost:50051', credentials.createInsecure());
  Client.hello(Request, (grpcErr, grpcRes) => {
    if (grpcErr) {
      res.status(500).json({});
    } else {
      const msg = grpcRes.getMessage();
      res.socket.server.io.emit('message', msg);
      res.status(200).json({});
    }
  });
}
