import type { NextApiRequest, NextApiResponse } from 'next';
import { HelloRequest, MessagesRequest } from '../../codegen/hello_pb';
import { GreetingClient } from '../../codegen/hello_grpc_pb';
import { credentials } from '@grpc/grpc-js';

type Props = {
  id: number;
  user_name: string;
  text: string;
};

export default function handler(req: NextApiRequest, res: NextApiResponse<Props[]>) {
  const Request = new MessagesRequest();
  const Client = new GreetingClient('localhost:50051', credentials.createInsecure());
  Client.messages(Request, (grpcErr, grpcRes) => {
    if (grpcErr) {
      res.status(500).json([]);
    } else {
      const msg = grpcRes.getMessagesList();
      res.status(200).json(msg.map((m) => ({ id: m.getId(), user_name: m.getUserName(), text: m.getText() })));
    }
  });
}
