import type { NextApiRequest, NextApiResponse } from 'next';
import { HelloRequest } from '../../codegen/hello_pb';
import { GreetingClient } from '../../codegen/hello_grpc_pb';
import { credentials } from '@grpc/grpc-js';
import { NextApiResponseServerIO } from './socket';

type Props = {
  msg: string;
};

export default function handler(req: NextApiRequest, res: NextApiResponseServerIO<Props>) {
  const x: Partial<{ name: string }> = req.query;
  const Request = new HelloRequest().setName(x.name || 'empty');
  const Client = new GreetingClient('localhost:50051', credentials.createInsecure());
  Client.hello(Request, (grpcErr, grpcRes) => {
    if (grpcErr) {
      res.status(500).json({ msg: 'ERROR' });
    } else {
      const msg = grpcRes.getMessage();
      res.status(200).json({ msg });
    }
  });
}
